# WeatherStack API Automation using BDD with "Cucumber" and "Serenity"
* This project is to automate tets for weather stack API  using BDD approach with Serenity framework

## Project Description:
* Project setup with Serenity Rest
* Written in Java with Junit, Cucumber & Maven
* Can run test scripts in parallel

## Setup:
* Install [Java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
* Install Maven [Maven](https://maven.apache.org/)

## Run tests:
* `mvn clean verify`

## GitLab CI Integration
Integrated with GitLab CI, new commit to code will trigger API test 
Report is generated as an artifact

![gitlab-ci](images/img.png)


--


![test](images/img_1.png)


--

## View HTML Report
* HTML report will be generated once execution finish 
* If running locally, reports are available at location :weather-api\target\site\serenity
* If using GitLab CI, reports are generated as artifact "serenity_report.zip". Please download zip. Location of report will be :target\site\serenity 
* Open Index.html in browser to see the reports

## Test Report screenshots

![testReport](images/img_2.png)

--

![testReportRequirement](images/img_3.png)

--

![WeatherAPItest](images/img_4.png)

--

![weatherAPIRestQuery](images/img_5.png)


## Writing new tests
To write a new test -
1) Add a scenario in weather_api.feature or create a new feature file
2) Apply changes in WeatherAPI step in applicable


## Weather Stack API detail
API Interaction document can be found on [Weatherstack](https://weatherstack.com/documentation)

