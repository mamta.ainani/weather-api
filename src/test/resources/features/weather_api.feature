Feature: Verify Get Current Weather Data


  Scenario Outline: Positive Scenario : Verify Current Weather API with Valid data
    Given The user have valid API key
    When The user sents GET request for "<city>"
    Then API should return status code as <status code>
    And Response body should contain "<city>" at location "<location>"

    Examples:
      | city      | status code | location      |
      | Amsterdam | 200         | location.name |
      | New York  | 200         | location.name |


  Scenario Outline: Negative Scenario : Verify Current Weather API with Invalid API key
    Given The user have invalid API key
    When The user sents GET request for "<city>"
    Then API should return status code as <status code>
    And Response body should contain <error code> at location "<error_code_location>"
    And Response body should contain "<message>" at location "<error_type_location>"
    
Examples:
      | status code | error code | message            | error_code_location | error_type_location|
      |  200        | 101        | invalid_access_key | error.code          | error.type         |






