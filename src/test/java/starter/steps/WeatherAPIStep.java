package starter.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.utils.Constants;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class WeatherAPIStep {

    @Steps

     String apiKey = "";
     static public Response response;

    @Given("^The user have valid API key$")
    public void the_user_have_proper_API_key() throws Exception {
        apiKey = Constants.API_KEY;
    }

    @Given("^The user have invalid API key$")
    public void the_user_has_invalid_API_key() throws Exception {
        apiKey = "123";
    }


    @When("^The user sents GET request for \"([^\"]*)\"$")
    public void the_user_sents_get_request_for(String city) {
        response = SerenityRest.get(Constants.ENDPOINTS+apiKey+"&query="+city);

    }


    @Then("^API should return status code as (\\d+)$")
    public void api_should_return_status_as(int statusCode) throws Exception {
        assertThat("Verify Status code for Weather Api ", response.getStatusCode(), equalTo(statusCode));
    }


    @Then("^Response body should contain \"([^\"]*)\" at location \"([^\"]*)\"$")
    public void response_should_have_city_at_location(String data, String location) throws Exception {
        JsonPath jsonPathEvaluator = response.jsonPath();
        assertThat(jsonPathEvaluator.get(location),equalTo(data));
    }

    @Then("^Response body should contain (\\d+) at location \"([^\"]*)\"$")
    public void response_should_have_city_at_location(int statusCode, String location) throws Exception {
        JsonPath jsonPathEvaluator = response.jsonPath();
        assertThat(jsonPathEvaluator.get(location),equalTo(statusCode));
    }

}
